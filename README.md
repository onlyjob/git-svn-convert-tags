## Utility to convert git-svn tag-branches to native git tags.

`git-svn` import SVN pseudo-tags as remote branches.
Sometimes it is desirable to convert such branches to native tags
but there is a catch: straightforward conversion produces tags outside of
main branch i.e. "master".
Often it is possible to find a "branch point" -- a particular commit that was
used to create an SVN tag.

[git-svn-convert-tags.sh](https://gitlab.com/onlyjob/git-svn-convert-tags/raw/master/git-svn-convert-tags.sh)
search for commit that is binary identical to remote branch HEAD,
tag it and remove remote branch.
