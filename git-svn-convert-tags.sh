#!/bin/bash
: <<=cut

=head1 NAME

git-svn-convert-tags.sh - convert git-svn pseudo-tags to GIT tags

=head1 SYNOPSYS

git-svn-convert-tags.sh [ B<-n> ] [ B<-0> ] [ B<tag_prefix> ]

=head1 OPTIONS

=over 4

=item B<-n>

Also set tags outside of current branch and delete corresponding
tag-branches.

=item B<-0>

Dry-run, do not change anything.

=back

=head1 DESCRIPTION

git-svn is not converting SVN tags to git tags.
Instead SNV tags are imported to git as remote branches.
This script tries to convert git-svn branches to tags assuming
I<--trunk=trunk>, see LAYOUT.

If running from git repository this script does the following:

  It eliminates "ghost" pseudo-tags (i.e. remote branches) ending
  with @NNNN if corresponding pseudo-tag contains exactly the same data.
  (Such "ghost" pseudo-tags are sometimes created after git-svn clone).

  It scans every commit of current branch to identify corresponding
  pseudo-tag (i.e. remote branch). All commits will be checked against
  pseudo-tags and if identical commit is found it will be tagged and
  matching pseudo-tag will be deleted.
  If successful this operation will make tags visible in the main history.
  (git-diff is used to compare remote branches HEAD and commits).

  If previous operation fail to find commit used to produce pseudo-tag a
  new tag will be applied to corresponding REF following by pseudo-tag
  (i.e. remote branch) removal. This step may be enabled with option "-n".
  Such tags will not be visible in main history but "gitk --all" may be
  used to visualise them.

Tags will retain their date and committer properties.

Optional argument is used to prepend new tags. For example if invoked as

  git-svn-convert-tags.sh debian/

tags will be named like "debian/2.0.1-1" which may be useful for
converting Debian package repositories from SVN.

=head1 LAYOUT

By default with I<--stdlayout> git-svn cloning SVN repository in a way
where all branches and tags are checked out to their own directories.
This is consuming too much space and usually it is much more effective to
clone using I<--trunk=trunk> like in the following example:

  git svn clone --trunk=trunk --stdlayout svn://svn.repositoty.null

In this example remote "trunk" will be merged to "master" branch.
It is not unusual for this layout to be much more compact than
default "--stdlayout".
The corresponding .git/config section will look like the following:

    [svn-remote "svn"]
        url = svn://svn.repositoty.null
        fetch = trunk:refs/remotes/trunk
        branches = branches/*:refs/remotes/*
        tags = tags/*:refs/remotes/tags/*

=head1 COPYRIGHT

Copyright: 2012-2013 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 ACKNOWLEDGEMENTS

Thanks to
  L<Me, BSD (blog)|http://mebsd.com/coding-snipits/bash-spinner-example-freebsd-loading-spinner.html>
for implementation of bash spinner.

=head1 DONATIONS

 Donations are much appreciated, please consider donating:

   Bitcoins : 15nCM6Rs4zoQKhBV55XxcPfwPKeAEHPbAn
  Litecoins : LZMLDNSfy3refx7bKQtEvPyYSbNHsfzLRZ
 AUD/PayPal : https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=onlyjob%40gmail%2ecom&lc=AU&currency_code=AUD

=head1 AVAILABILITY

  Home page : https://gitlab.com/onlyjob/git-svn-convert-tags
     Script : https://gitlab.com/onlyjob/git-svn-convert-tags/raw/master/git-svn-convert-tags.sh

=head1 VERSION

2013.0113

=cut

while [ -n "$1" ]; do
    if [ "$1" = "-h" -o "$1" = "--help" ]; then
        [ $(which perldoc) ] && perldoc $0 \
        || head -103 $0
        exit 0
    elif [ "$1" = "-n" ]; then
        ON=1
    elif [ "$1" = "-0" ]; then
        NO=1
    elif [ ! "${1:0:1}" = "-" ]; then
        TAG_PREFIX="$1"
    else
        echo -e "E: unknown option. Usage:"
        echo "    $(basename $0) [ -n ] [ -0 ] [ tag_prefix ]"
        exit 1
    fi
    shift
done

# spinner
i=1;
sp="/-\|";

function tag_it {
    local GIT_COMMIT_MSG="$(git log -1 --pretty=format:"%s%n%b" "$2")"
    GIT_COMMITTER_EMAIL="$(git log -1 --pretty=format:"%ce" "$2")" \
    GIT_COMMITTER_NAME="$(git log -1 --pretty=format:"%cn" "$2")" \
    GIT_COMMITTER_DATE="$(git log -1 --pretty=format:"%ad" "$2")" \
    git tag -m "${GIT_COMMIT_MSG}" "${TAG_PREFIX}$1" "$2"
}

git branch -r | sort --reverse |
while read BRANCH; do
    # processing only SVN tags
    if [[ "${BRANCH}" =~ "tags/" ]]; then
        TAG=${BRANCH##*/}
    else
        continue
    fi
    [ "${TAG}" = "trunk" ] && continue          # paranoidly avoid processing "trunk" by accident
    echo -n ":: ${TAG} "

    # check for redundant "ghost" branches
    if [ ! "${BRANCH}" = "${BRANCH%%@*}" ] \
    && [ -z "$(git diff remotes/${BRANCH} remotes/${BRANCH%%@*} --name-only)" ]; then
        # removing redundant branch ending with /@[0-9]+/ i.e.
        # drop branch '2.0-1@10908' if no different from '2.0-1'.
        echo -e " Removing redundant branch ${BRANCH}."
        [ "${NO}" = "1" ] && continue
        git branch -r -d "${BRANCH}"
        continue
    fi

    git log --pretty=format:"%H" |
    while read H; do
        printf "%s\b" "${sp:i++%${#sp}:1}"       # spinner
        # idea: diff output can be filtered with
        # grep -v ^CVSROOT/
        # to exclude irrelevant changes
        [ -n "$(git diff remotes/${BRANCH} $H --name-only)" ] \
        && continue
        echo -e " Found matching commit."
        [ "${NO}" = "1" ] && break
        echo -n "   "  # pad the output of next command
        tag_it "${TAG}" "$H" \
        && git branch -r -d "${BRANCH}"
        break
    done

    git show-ref --quiet "remotes/${BRANCH}"    # check if branch still exists
    if [ $? -eq 0 ]; then
        [ "${NO}" = "1" ] && continue
        echo -e " No matching commit."
        [ ! "${ON}" = "1" ] && continue
        echo "Tagging outside of current branch."
        [ "${NO}" = "1" ] && continue
        tag_it "${TAG}" "remotes/${BRANCH}" \
        && git branch -r -d "${BRANCH}"
    fi
done

exit
